import cv2
import os
import imageio
import numpy as np
sourceDir= './new_images'
image_extension = '.png'
targetDir= './new_images'
color = [128 , 0, 0]

if __name__ == '__main__':
    if not os.path.exists(targetDir):
        os.makedirs(targetDir)

    # import images
    image_names = []
    for file in os.listdir(sourceDir):
        if file.endswith(image_extension):
            image_names.append(file)
    print(image_names)

    for image_path in image_names:

        # image = imageio.imread(os.path.join(sourceDir, image_path))
        print(image_path)
        image = cv2.imread(os.path.join(sourceDir, image_path),cv2.IMREAD_COLOR)

        color = np.array(color, dtype = "uint8")
        # upper = np.array(color, dtype = "uint8")

    	# find the colors within the specified boundaries and apply
    	# the mask
        mask = cv2.inRange(image, color, color)
        # output = cv2.bitwise_and(image, image, mask = mask)

        cv2.imwrite(os.path.join(targetDir,image_path),mask)
