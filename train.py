from keras import optimizers
from keras.callbacks import ModelCheckpoint
from keras.callbacks import TensorBoard
from keras.callbacks import LearningRateScheduler
from keras.callbacks import EarlyStopping
from keras.utils import multi_gpu_model
from keras.models import load_model
from keras import backend as K
import tensorflow as tf
import os
from utils import PolyDecay
from utils import Generator
import model
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
chk= True
batch_size   = 32                     # 'input batch size'
image_width  = 512                    # 'the input image width'
image_height = 512                    # 'the input image height'
lr           = 0.001                   # 'learning rate of the optimizer'
decay        = 0.9                    # 'learning rate decay (per epoch)'
n_epochs     = 300                   # 'number of epochs to train for'
n_gpu        = 6                      # 'number of GPUs to use'
n_cpu        = 1                      # 'number of CPU threads to use during data generation'
check   = 'output/Bench.h5' # 'path to model checkpoint to resume training'
check   ='output/crackfull+1set2001.h5'
epoch        = 0                  # 'epoch to start training'
colormode    = 'grayscale'            #'Input Images color'
if colormode=='grayscale':
    channels=1
else:
	channels=3
DataDir='/data/mert/crackfull'
DataDir='C:/Users/mergin/Documents/GitHub/ICNet/datasets/'
SetName= 'crackfull+1set2'
DataDir=DataDir+SetName

# Workaround to forbid tensorflow from crashing the gpu
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
K.set_session(sess)

# Callbacks
checkpoint = ModelCheckpoint('output/weights/'+SetName+'{epoch:03d}.h5', monitor='loss',save_best_only=True, mode='min')
# checkpoint = ModelCheckpoint('output/weights.h5', monitor='val_loss', verbose=0, save_best_only=True, save_weights_only=False, mode='auto', period=1)
tensorboard = TensorBoard(log_dir='./logs',batch_size=batch_size)
lr_decay = LearningRateScheduler(PolyDecay(lr, decay, n_epochs).scheduler)
ES=EarlyStopping(monitor='loss', min_delta=0, patience=90, verbose=1, mode='auto', baseline=None, restore_best_weights=True)



# Generators
train_generator = Generator(folder=DataDir,\
batch_size=batch_size, crop_shape=None,resize_shape=(image_width, image_height),\
n_classes=2,colormode=colormode)
val_generator = Generator(folder=DataDir,\
mode='val', batch_size=batch_size, crop_shape=None,\
 resize_shape=(image_width, image_height),n_classes=2)

# Optimizer
# optim = optimizers.SGD(lr=lr, momentum=0.9)
optim = optimizers.Nadam(lr=lr, beta_1=0.99, beta_2=0.999, schedule_decay=1e-4)


if chk:
    net = load_model(check ,  custom_objects={"tf": tf})
# Model
else:
    net = model.build_bn(image_width, image_height, 2, train=True,colormode=channels)


# Training
net.compile(optim, 'categorical_crossentropy', \
loss_weights=[1.0, 0.4, 0.16], metrics=['mae', 'acc'])
net.fit_generator(train_generator, len(train_generator)+10,   n_epochs, callbacks=[ checkpoint,tensorboard, lr_decay,ES],
                    validation_data=val_generator, validation_steps=len(val_generator), workers=n_gpu,
                    use_multiprocessing=False, shuffle=True, max_queue_size=10, initial_epoch=epoch)
###############
