# Keras-ICNet
### [[paper]](https://arxiv.org/abs/1704.08545)

Keras implementation of Real-Time Semantic Segmentation on High-Resolution Images. **Training in progress!**
    
## Requisites
- Python 3.6.3
- Keras 2.1.1 with Tensorflow backend
- A dataset, with labels as images.

## Train
Issue ```./train --help``` for options to start a training session, default arguments should work out-of-the-box.

You need to place the dataset following the next directory convention:

    .
    ├── dataset                   
    |   ├── train
    |   |   ├── images             # Contains the input images
    |   |   └── instances          # Contains the target labels
    |   ├── val
    |   |   ├── images
    |   |   └── instances
    |   └── test
    |   |   └── images
    

## Logging
Use tensorboard with following command 
    tensorboard --logdir=./logs
