
import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import numpy as np
from keras import backend as K
from keras.utils import multi_gpu_model
from keras.models import load_model
import tensorflow as tf


from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.backend.tensorflow_backend import set_session
import cv2
import skimage.io as io
import skimage.transform as trans
from keras.models import load_model
from time import perf_counter

from kito import reduce_keras_model
from keras.preprocessing.image import ImageDataGenerator
checkpoint = 'output'+os.sep+'crackfull+1set2273.h5'   # 'path to model checkpoint'
from utils import *


infer = True
# Workaround to forbid tensorflow from crashing the gpu
if infer:
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    K.set_session(sess)

test_batch_size=32
target_size = (512,512)
# target_size = (640,1280)
speedTest=False

as_gray=True
color_mode= "grayscale"
if infer:
    # Load Model
    if checkpoint:
        model = load_model(checkpoint, custom_objects={"tf": tf})
    else:
        print('No checkpoint specified! Set it with the --checkpoint argument option')
        exit()

# Data generator of keras
test_datagen = ImageDataGenerator()


# Set IO directories
drivename = 'data/mert'
drivename='C:/Users/mergin/Documents/GitHub/ICNet/datasets'
dirname ='crackset2'

test_path= drivename + os.sep + dirname+os.sep+'test'
testx_path= test_path + os.sep +'image'
testy_path= test_path + os.sep +'label'
PredictDir=test_path+os.sep+'results'+os.sep
#INFERENCE
imgset=np.empty((0,)+target_size+(1,))
imgnames= []
avg_time_list=[]

for file in os.listdir(testx_path):
    img = cv2.imread(os.path.join(testx_path,file), 0)
    initShape= img.shape
    img = cv2.resize(img, tuple(reversed(target_size)))

    img = np.reshape(img,(1,)+img.shape+(1,))

    imgset=np.append(imgset,img,axis=0)
    imgnames.append(file)

if infer:
    if speedTest:
        for batch in test_datagen.flow(imgset,batch_size=test_batch_size,shuffle=False):

            start = perf_counter()

            results = model.predict(batch)
            t=perf_counter() - start
            # avg_time_list.append(t)
            # avg_time=np.mean(avg_time_list[-50:])

            print('Generated %d segmentations in %1.6f secs -- %2.3f mSecPerFrame'\
             % (len(batch),t, t/len(batch)*1000))

            # results=np.argmax(results[0], axis=-1)[:,:,:,np.newaxis]*255
            # print(results.shape)
            saveResult(PredictDir,results[0],flag_multi_class = False,filenames=imgnames,initShape=initShape)
    else:
        start = perf_counter()
        results = model.predict(imgset)
        t=perf_counter() - start

        print('Generated %d segmentations in %1.6f secs -- %2.3f mSecPerFrame'\
         % (len(imgset),t, t/len(imgset)*1000))

        saveResult(PredictDir,results[0],flag_multi_class = False,filenames=imgnames,initShape=initShape)

compareResult(ImageDir=testx_path,GroundDir=testy_path,\
PredictDir=PredictDir,SaveDir=test_path+os.sep+'visualization')
