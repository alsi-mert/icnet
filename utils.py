import os
import glob
import random
import json
import pickle
import gc

import numpy as np
import cv2
import matplotlib.pyplot as plt
from keras.utils import to_categorical
from keras.callbacks import Callback
from keras.layers import concatenate
from keras.layers.core import Lambda
from keras.models import Model
from keras.utils.data_utils import Sequence
from keras import backend as K
import skimage.io as io
import tensorflow as tf
Sky = [128,128,128]
Building = [128,0,0]
Pole = [192,192,128]
Road = [128,64,128]
Pavement = [60,40,222]
Tree = [128,128,0]
SignSymbol = [192,128,128]
Fence = [64,64,128]
Car = [64,0,128]
Pedestrian = [64,64,0]
Bicyclist = [0,128,192]
Unlabelled = [0,0,0]

COLOR_DICT = np.array([Sky, Building, Pole, Road, Pavement,
                          Tree, SignSymbol, Fence, Car, Pedestrian, Bicyclist, Unlabelled])

class Generator(Sequence):
    def __init__(self, folder='datasets/set', mode='train', n_classes=2, batch_size=32, resize_shape=None,
                 crop_shape=(256, 256), horizontal_flip=True, vertical_flip=True, brightness=0.3, rotation=6.0, zoom=None, colormode='grayscale'):

        self.image_path_list = sorted(glob.glob(os.path.join(folder, mode, 'image/*')))
        self.label_path_list = sorted(glob.glob(os.path.join(folder, mode, 'label/*')))
        self.mode = mode
        self.n_classes = n_classes
        self.batch_size = batch_size
        self.resize_shape=resize_shape
        self.crop_shape = crop_shape
        self.horizontal_flip = horizontal_flip
        self.vertical_flip = vertical_flip
        self.brightness = brightness
        self.rotation = rotation
        self.zoom = zoom
        if colormode=='grayscale':
            self.colorflag= 0
            self.colordim=1
        else:
            self.colorflag= 1
            self.colordim=3
        # Preallocate memory
        if mode == 'train' and self.crop_shape:
            self.X = np.zeros((batch_size, crop_shape[1], crop_shape[0], self.colordim), dtype='float32')
            self.Y1 = np.zeros((batch_size, crop_shape[1]//4, crop_shape[0]//4, self.n_classes), dtype='float32')
            self.Y2 = np.zeros((batch_size, crop_shape[1]//8, crop_shape[0]//8, self.n_classes), dtype='float32')
            self.Y3 = np.zeros((batch_size, crop_shape[1]//16, crop_shape[0]//16, self.n_classes), dtype='float32')
        elif self.resize_shape:
            self.X = np.zeros((batch_size, resize_shape[1], resize_shape[0], self.colordim), dtype='float32')
            self.Y1 = np.zeros((batch_size, resize_shape[1]//4, resize_shape[0]//4, self.n_classes), dtype='float32')
            self.Y2 = np.zeros((batch_size, resize_shape[1]//8, resize_shape[0]//8, self.n_classes), dtype='float32')
            self.Y3 = np.zeros((batch_size, resize_shape[1]//16, resize_shape[0]//16, self.n_classes), dtype='float32')
        else:
            raise Exception('No image dimensions specified!')

    def __len__(self):
        return len(self.image_path_list) // self.batch_size

    def __getitem__(self, i):
        for n, (image_path, label_path) in enumerate(zip(self.image_path_list[i*self.batch_size:(i+1)*self.batch_size],
                                                        self.label_path_list[i*self.batch_size:(i+1)*self.batch_size])):

            image = cv2.imread(image_path, 1)
            label = cv2.imread(label_path, 0)
            if not self.colorflag:
                image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                image = image[:, :, np.newaxis]

            if self.resize_shape:
                image = cv2.resize(image, self.resize_shape)
                label = cv2.resize(label, self.resize_shape)

            # Do augmentation (only if training)
            if self.mode == 'train':
                if self.horizontal_flip and random.randint(0,1):
                    image = cv2.flip(image, 1)
                    label = cv2.flip(label, 1)
                if self.vertical_flip and random.randint(0,1):
                    image = cv2.flip(image, 0)
                    label = cv2.flip(label, 0)
                if self.brightness:
                    factor = 1.0 + abs(random.gauss(mu=0.0, sigma=self.brightness))
                    if random.randint(0,1):
                        factor = 1.0/factor
                    table = np.array([((i / 255.0) ** factor) * 255 for i in np.arange(0, 256)]).astype(np.uint8)
                    image = cv2.LUT(image, table)
                if self.rotation:
                    angle = random.gauss(mu=0.0, sigma=self.rotation)
                else:
                    angle = 0.0
                if self.zoom:
                    scale = random.gauss(mu=1.0, sigma=self.zoom)
                else:
                    scale = 1.0
                if self.rotation or self.zoom:
                    M = cv2.getRotationMatrix2D((image.shape[1]//2, image.shape[0]//2), angle, scale)
                    image = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
                    label = cv2.warpAffine(label, M, (label.shape[1], label.shape[0]))
                if self.crop_shape:
                    image, label = _random_crop(image, label, self.crop_shape)
            if not self.colorflag:
                image= image[:,:,np.newaxis]
            self.X[n] = image

            labelnos=np.unique(label)

            LUT=np.zeros((256))
            classes=list(range(self.n_classes))
            for idx,value in enumerate(labelnos):
                LUT[value]=classes[np.amin([idx,self.n_classes-1])]
            label=cv2.LUT(label,LUT)
            self.Y1[n] = to_categorical(cv2.resize(label, (label.shape[1]//4, label.shape[0]//4)), self.n_classes).reshape((label.shape[0]//4, label.shape[1]//4, -1))
            self.Y2[n] = to_categorical(cv2.resize(label, (label.shape[1]//8, label.shape[0]//8)), self.n_classes).reshape((label.shape[0]//8, label.shape[1]//8, -1))
            self.Y3[n] = to_categorical(cv2.resize(label, (label.shape[1]//16, label.shape[0]//16)), self.n_classes).reshape((label.shape[0]//16, label.shape[1]//16, -1))

        return self.X, [self.Y1, self.Y2, self.Y3]

    def on_epoch_end(self):
        # Shuffle dataset for next epoch
        c = list(zip(self.image_path_list, self.label_path_list))
        random.shuffle(c)
        self.image_path_list, self.label_path_list = zip(*c)

        # Fix memory leak (Keras bug)
        gc.collect()

class Visualization(Callback):
    def __init__(self, resize_shape=(640, 320), batch_steps=10, n_gpu=1, **kwargs):
        super(Visualization, self).__init__(**kwargs)
        self.resize_shape = resize_shape
        self.batch_steps = batch_steps
        self.n_gpu = n_gpu
        self.counter = 0

        # TODO: Remove this lazy hardcoded paths
        self.test_images_list = glob.glob('datasets/mapillary/test/images/*')
        with open('datasets/mapillary/config.json') as config_file:
            config = json.load(config_file)
        self.labels = config['labels']


    def on_batch_end(self, batch, logs={}):
        self.counter += 1

        if self.counter == self.batch_steps:
            self.counter = 0

            test_image = cv2.resize(cv2.imread(random.choice(self.test_images_list), 1), self.resize_shape)

            inputs = [test_image]*self.n_gpu
            output, _, _ = self.model.predict(np.array(inputs), batch_size=self.n_gpu)

            cv2.imshow('input', test_image)
            cv2.waitKey(1)
            cv2.imshow('output', apply_color_map(np.argmax(output[0], axis=-1), self.labels))
            cv2.waitKey(1)

class PolyDecay:
    def __init__(self, initial_lr, power, n_epochs):
        self.initial_lr = initial_lr
        self.power = power
        self.n_epochs = n_epochs

    def scheduler(self, epoch):
        return self.initial_lr * np.power(1.0 - 1.0*epoch/self.n_epochs, self.power)

class ExpDecay:
    def __init__(self, initial_lr, decay):
        self.initial_lr = initial_lr
        self.decay = decay

    def scheduler(self, epoch):
        return self.initial_lr * np.exp(-self.decay*epoch)

# Taken from Mappillary Vistas demo.py
def apply_color_map(image_array, labels):
    color_array = np.zeros((image_array.shape[0], image_array.shape[1], 3), dtype=np.uint8)

    for label_id, label in enumerate(labels):
        # set all pixels with the current label to the color of the current label
        color_array[image_array == label_id] = label["color"]

    return color_array

def _random_crop(image, label, crop_shape):
    if (image.shape[0] != label.shape[0]) or (image.shape[1] != label.shape[1]):
        raise Exception('Image and label must have the same dimensions!')

    if (crop_shape[0] < image.shape[1]) and (crop_shape[1] < image.shape[0]):
        x = random.randrange(image.shape[1]-crop_shape[0])
        y = random.randrange(image.shape[0]-crop_shape[1])

        return image[y:y+crop_shape[1], x:x+crop_shape[0], :], label[y:y+crop_shape[1], x:x+crop_shape[0]]
    else:
        raise Exception('Crop shape exceeds image dimensions!')

def labelVisualize(num_class,color_dict,img):
    img = img[:,:,0] if len(img.shape) == 3 else img
    img_out = np.zeros(img.shape + (3,))
    for i in range(num_class):
        img_out[img == i,:] = color_dict[i]
    return img_out / 255



def saveResult(save_path,npyfile,flag_multi_class = False,num_class = 2,filenames=False,initShape=None):
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    for i,item in enumerate(npyfile):
        img = labelVisualize(num_class,COLOR_DICT,item) if flag_multi_class else item[:,:,0]
        # img=img*255
        if initShape:
            img = cv2.resize(img, tuple(reversed(initShape)))
        if filenames:
            io.imsave(os.path.join(save_path,"%s_predict.jpg"%os.path.splitext(filenames[i])[0]),img)
        else:
            io.imsave(os.path.join(save_path,"%d_predict.jpg"%i),img)
def compareResult(ImageDir,GroundDir,PredictDir,SaveDir):
    import matplotlib.pyplot  as plt

    if not os.path.exists(SaveDir):
        os.makedirs(SaveDir)

    from skimage.color import rgb_colors
    DEFAULT_COLORS = ('white', 'blue', 'red', 'green')
    DEFAULT_COLOR_TUPLE=[]
    color_dict = {k: v for k, v in rgb_colors.__dict__.items()
              if isinstance(v, tuple)}
    for color in DEFAULT_COLORS:
        DEFAULT_COLOR_TUPLE.append(color_dict[color])

    for file in os.listdir(PredictDir):
        Basefilename=file.split('_predict')[0]

        defext=os.path.splitext(file)[-1]
        Gfilename=Basefilename+defext
        # print(Gfilename,defext,os.listdir(GroundDir))
        for Gfilename in os.listdir(GroundDir):
            if Basefilename==os.path.splitext(Gfilename)[0]:
                # print('Found',Basefilename)
                label_viz=drawResult(ImageDir+os.sep+Basefilename+defext,
                GroundDir+os.sep+Gfilename,
                PredictDir+os.sep+file,colors=DEFAULT_COLOR_TUPLE,
                SaveDir=SaveDir+os.sep+Basefilename)
                plt.savefig(SaveDir+os.sep+Basefilename+defext, dpi = 1000,bbox_inches='tight', pad_inches=0)


        #


def drawResult(ImageDir,GroundTruthDir,PredictionDir, colors=None,SaveDir=None):
    from skimage.color import label2rgb
    from skimage.transform import resize
    from skimage.filters import threshold_otsu
    from skimage.filters import try_all_threshold
    import imageio
    import warnings
    warnings.filterwarnings("ignore")

    Image=io.imread(ImageDir,as_gray=False)
    GroundTruth=io.imread(GroundTruthDir)
    Prediction=io.imread(PredictionDir)
    if GroundTruth.shape!=Prediction.shape:
        if np.prod(Prediction.shape)<np.prod(GroundTruth.shape):
            Prediction=resize(Prediction,GroundTruth.shape,mode='symmetric')
        else:
            GroundTruth=resize(GroundTruth,Prediction.shape,mode='symmetric')
    # print(sum(Prediction<0.9))
    # print(np.unique(Prediction))
    # print(sum(1-Prediction[Prediction<254]))
    # if np.sum(Prediction[Prediction<250])>10:
    #     thresh = threshold_otsu(1-Prediction[Prediction<255])
    # else:
    #     thresh = 1
    # fig, ax =try_all_threshold(Prediction, figsize=(10, 8), verbose=False)

    # plt.savefig(PredictionDir+'.jpg', dpi = 1000,bbox_inches='tight', pad_inches=0)
    # plt.cla()
    # plt.close()
    thresh=70
    PredictionBinary = (Prediction < thresh)*1

    # io.imsave('PredictionBinary.png',PredictionBinary*255)


    GroundTruthBinary = (GroundTruth > 0.5)*1
    MappedImage=2*GroundTruthBinary+PredictionBinary
    VIS=label2rgb(MappedImage,image=Image, alpha=0.3, colors=colors, kind='overlay')


    Size = np.prod(VIS.shape[0:2])
    # PixList=list(zip (VIS[:,:,0].flatten(),VIS[:,:,1].flatten(),VIS[:,:,2].flatten()))

    TNRatio = np.sum(MappedImage == 0) / Size
    FPRatio = np.sum(MappedImage == 1) / Size
    FNRatio = np.sum(MappedImage == 2) / Size
    TPRatio = np.sum(MappedImage == 3) / Size

    label = np.flatnonzero(np.array([TNRatio,FPRatio,FNRatio,TPRatio]) >0.0001).astype(int).tolist()

    label_names = ['True Negative %3f' %TNRatio,\
                'False Positive %3f' %FPRatio,\
                'False Negative %3f' %FNRatio,\
                'True Positive %3f' %TPRatio]
    fig, ax = plt.subplots()
    plt.subplot(311)
    plt.imshow(Image,cmap='gray')
    plt.axis('off')
    #
    plt_handlers = []
    plt_titles = []
    plt.subplot(312)
    plt.imshow(Image,cmap='gray')
    vmin=np.amin(1-Prediction)
    vmax=np.amax(1-Prediction)
    print(vmin,thresh,vmax)
    plt.imshow(255-Prediction, cmap=plt.cm.GnBu, alpha=.8)

    io.imsave(SaveDir+'segFull.png',Prediction)
    io.imsave(SaveDir+'segFullThresh.png',(1-PredictionBinary).astype(np.uint8)*255)

    plt.axis('off')
    #
    plt_handlers = []
    plt_titles = []
    cax = plt.axes([0.9, 0.4, 0.4, 0.04])

    cbar = plt.colorbar(cax=cax,orientation='horizontal')

    plt.axvline((thresh-vmin), c='k') # my data is between 0 and 1

    cax = plt.axes([0.9, 0.5, 0.4, 0.4])
    plt.hist(1-Prediction[Prediction<255],20)

    plt.subplot(313)
    plt.imshow(1-PredictionBinary,cmap='gray')
    plt.axis('off')
    #
    plt_handlers = []
    plt_titles = []
    # for label_value, label_name in enumerate(label_names):

    #     if label_value not in label:
    #         continue
    #     fc = colors[label_value]
    #     p = plt.Rectangle((0, 0), 1, 1, fc=fc)
    #     plt_handlers.append(p)
    #     plt_titles.append('{value}: {name}'
    #                       .format(value=label_value, name=label_name))
    # plt.legend(plt_handlers, plt_titles, bbox_to_anchor=(1,0,0.5, 1), loc='center right',borderaxespad=0. ,framealpha=.5)

    return
def visualizeHistory(history,SaveDir):
    print(history.history.keys())
    # Plot training & validation accuracy values
    plt.plot(history.history['conv6_cls_acc'])
    plt.plot(history.history['val_conv6_cls_acc'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.savefig(SaveDir+os.sep+'Accuracy.png', dpi = 1000,bbox_inches='tight', pad_inches=0)

    # Plot training & validation loss values
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.savefig(SaveDir+os.sep+'Loss.png', dpi = 1000,bbox_inches='tight', pad_inches=0)
